# Employee REST API

## Description

This application provides a RESTful API for interacting with employees, allows for basic operations as:

- Create new employee
- Modify Employee
- Mark an employee as inactive
- Find an employee by its ID
- Get all the active employees
- Get all the inactive employees

An employee has the following properties

```
ID - Unique identifier for an employee
FirstName - Employees first name
MiddleInitial - Employees middle initial
LastName - Employee last name
DateOfBirth - Employee birthday and year
DateOfEmployment - Employee start date
Status - ACTIVE or INACTIVE
```

## Technical Overview

This application was made using the Spring Initializr tool for VSCode and as such is a Spring Boot app and has the following:

- Spring REST implementation
- Spring MVC
- Java Persistence API
- Spring Data
- Spring Security

And makes use of the embebed Tomcat servlet container and H2 DB.

### Justification

As part of the building process some patterns and design choices where made and some of them are justified below:

- **Architectural pattern MVC:** This was chosen because it makes a perfect fit for this type of applications allowing to easily separate the view form all the logic behind.
- **Design Pattern DAO:** Data-Access-Object was used because it allows to isolate the persistence layer on a given application, by isolating this layer we gain the ability to change how objects are persisted an to where, this also helps to achieve low coupling, high cohesion.
- **Design Pattern Singleton:** By default spring beans and controllers are singleton, on an API that is receiving many calls this helps by reducing resources usage as the application only uses one instance of a class at a time, one must be careful while using this, as is possible to keep data in between request.

## Usage

### Starting the application

To run the application simply run the `mvnw spring-boot:run` command on the command line while at the root directory of the application.

### Interacting with the API

Once the application has successfully started the API should be at http://localhost:8080/

From there all REST methods are exposed at [/employee](http://localhost:8080/employee)

### Examples

#### Get All

GET http://localhost:8080/employee

Result:

```json
[
    {
        "id": 1,
        "firstName": "Carlos",
        "middleInitial": "A",
        "lastName": "Pinedo",
        "dateOfBirth": "1991-10-07",
        "dateOfEmployment": "2019-08-29",
        "status": true
    }
]
```

#### Find by ID

GET http://localhost:8080/employee/1

Result:

```json
{
    "id": 1,
    "firstName": "Carlos",
    "middleInitial": "A",
    "lastName": "Pinedo",
    "dateOfBirth": "1991-10-07",
    "dateOfEmployment": "2019-08-29",
    "status": true
}
```

#### Update

PUT http://localhost:8080/employee/:id

Json to send

```json
{
    "id": null,
    "firstName": "Juan",
    "middleInitial": "L",
    "lastName": "Orozco",
    "dateOfBirth": "1992-05-15",
    "dateOfEmployment": "2019-08-29",
    "status": true
}
```

#### Create

POST http://localhost:8080/employees/

Json to send:

```json
{
    "id": null,
    "firstName": "Manuel",
    "middleInitial": "S",
    "lastName": "Rodriguez",
    "dateOfBirth": "1990-01-22",
    "dateOfEmployment": "2019-08-29",
    "status": null
}
```

#### Delete

DELETE http://localhost:8080/employees/1

This call uses Basic Authentication with Spring, the following credentials can be used:

- **User:** `admin`
- **Password** `adminPass`

The following credentials are also available and should not allow the use of this call:

- **User:** `user`
- **Password** `userPass`

### Pre-loading users in the application

The application looks for a json file in its root directory which should contain the following estructure (an example file is provided):

```json
[
    {
        "employee": {
            "firstName": "Carlos",
            "lastName": "Pinedo",
            "middleInitial": "A",
            "dateOfEmployment": "2019-08-29",
            "dateOfBirth": "1991-10-07",
            "id": 1,
            "status": true
        }
    },
    {
        "employee": {
            "firstName": "Manuel",
            "lastName": "Rodriguez",
            "middleInitial": "S",
            "dateOfEmployment": "2019-08-29",
            "dateOfBirth": "1991-10-07",
            "id": 2,
            "status": true
        }
    }
]
```
