package com.example.kenzan.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.LinkedList;
import java.util.List;

import com.example.kenzan.model.Employee;
import com.example.kenzan.repository.EmployeeRepository;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is used for data base configuration and is automatically run at
 * the start of the application.
 */
@Configuration
class LoadDatabase {

    /**
     * This method pre-loads the H2 embebed data base with a set of pre made
     * employees.
     * 
     * @param repository
     * @return
     */
    @Bean
    CommandLineRunner initDatabase(EmployeeRepository repository) {
        return args -> {
            JSONParser parser = new JSONParser();
            try (FileReader reader = new FileReader("employees.json")) {
                Object object = parser.parse(reader);
                JSONArray employeeList = (JSONArray) object;
                employeeList.forEach(e -> parseEmployee((JSONObject) e, repository));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        };
    }

    /**
     * Helper that parses a {@link JSONObject} into a {@link Employee}.
     * 
     * @param employee
     * @param repository
     */
    private static void parseEmployee(JSONObject employee, EmployeeRepository repository) {
        JSONObject employeeObject = (JSONObject) employee.get("employee");
        Employee tmp = new Employee((String) employeeObject.get("firstName"),
                (String) employeeObject.get("middleInitial"), (String) employeeObject.get("lastName"),
                LocalDate.parse((String) employeeObject.get("dateOfBirth")),
                LocalDate.parse((String) employeeObject.get("dateOfEmployment")));
        tmp.setId((Long) employeeObject.get("id"));
        tmp.setStatus((boolean) employeeObject.get("status"));
        System.out.println(repository.save(tmp));
    }

    /**
     * This method was initially used to generate a JSON file with {@link Employee}
     * records.
     * 
     * @param repository
     */
    private static void generateJson(EmployeeRepository repository) {
        System.out.println("users generated");
        List<Employee> employees = new LinkedList<>();
        JSONArray employeeList = new JSONArray();
        employees.add(repository
                .save(new Employee("Carlos", "A", "Pinedo", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("Manuel", "S", "Rodriguez", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("Juan", "L", "Orozco", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("Pedro", "N", "Garcia", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("Luisa", "A", "Jimenez", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("David", "F", "Rivera", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("German", "L", "Garcia", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("Omar", "E", "Sanchez", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.add(repository
                .save(new Employee("Luis", "P", "Cruz", LocalDate.of(1991, Month.OCTOBER, 7), LocalDate.now())));
        employees.stream().forEach(e -> {
            JSONObject employeeDetails = new JSONObject();
            employeeDetails.put("id", e.getId());
            employeeDetails.put("firstName", e.getFirstName());
            employeeDetails.put("middleInitial", e.getMiddleInitial());
            employeeDetails.put("lastName", e.getLastName());
            employeeDetails.put("dateOfBirth", e.getDateOfBirth().toString());
            employeeDetails.put("dateOfEmployment", e.getDateOfEmployment().toString());
            employeeDetails.put("status", e.isStatus());
            System.out.println(e);
            JSONObject employeeObject = new JSONObject();
            employeeObject.put("employee", employeeDetails);
            employeeList.add(employeeObject);
        });
        try (FileWriter file = new FileWriter("employees.json")) {
            file.write(employeeList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}