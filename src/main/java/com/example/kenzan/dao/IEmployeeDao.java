package com.example.kenzan.dao;

import java.util.List;
import java.util.Optional;

import com.example.kenzan.model.Employee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * This interface extends from {@link JpaRepository} and handles al data access
 * for the employee entity.
 */
public interface IEmployeeDao extends JpaRepository<Employee, Long> {

    /**
     * Marks a given record as deleted by changing the status field to false.
     * 
     * @param id
     */
    @Modifying
    @Transactional
    @Query("update #{#entityName} e set e.status=false where e.id=?1")
    public void softDelete(Long id);

    /**
     * Returns all records that are marked as deleted.
     * 
     * @return
     */
    @Query("select e from #{#entityName} e where e.status=false")
    public List<Employee> recycleBin();

    /**
     * Returns all records that are not marked as deleted.
     * @return 
     */
    @Override
    @Query("select e from #{#entityName} e where e.status = true")
    List<Employee> findAll();

    /**
     * Returns the record that corresponds to the given ID as long as is not marked
     * as deleted.
     */
    @Override
    @Query("select e from #{#entityName} e where e.id = ?1 and e.status = true")
    Optional<Employee> findById(Long id);
}