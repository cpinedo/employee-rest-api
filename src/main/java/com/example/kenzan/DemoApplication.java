package com.example.kenzan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class is used to start the application and makes use of spring boot
 * auto-configuration feature to handle the MVC, REST, JPA, Tomcat and H2 needs.
 * 
 * @author Carlos Alberto Pinedo García
 */
@SpringBootApplication
public class DemoApplication {

  /**
   * This main method starts the application which can be run by using the "mvnw
   * spring-boot:run" command.
   * 
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }

}
