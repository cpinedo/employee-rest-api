package com.example.kenzan.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.example.kenzan.dao.IEmployeeDao;
import com.example.kenzan.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class handles REST request and acts as controller for the application.
 */
@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private IEmployeeDao employeeDao;

    /**
     * Returns a List containing all the employees available.
     * 
     * @return
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> getAllEmployees() {
        return StreamSupport.stream(employeeDao.findAll().spliterator(), false).collect(Collectors.toList());
    }

    /**
     * Returns an employee matching the given ID.
     * 
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Employee getEmployee(@PathVariable Long id) {
        return employeeDao.findById(id).get();
    }

    /**
     * Checks if an employee already exist and updates it.
     * 
     * @param employee
     * @param id
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
        if (employeeDao.existsById(id)) {
            employee.setId(id);
            employee.setStatus(true);
            employeeDao.save(employee);
        }
    }

    /**
     * Marks an employee for deletion.
     * 
     * @param id
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEmployee(@PathVariable Long id) {
        employeeDao.softDelete(id);
    }

    /**
     * Creates a new employee record.
     * 
     * @param employee
     * @return
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee) {
        return employeeDao.save(new Employee(employee.getFirstName(), employee.getMiddleInitial(),
                employee.getLastName(), employee.getDateOfBirth(), employee.getDateOfEmployment()));
    }

    /**
     * Returns a list of all employees marked as deleted.
     * 
     * @return
     */
    @GetMapping(value = "/deleted", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> deleted() {
        return employeeDao.recycleBin();
    }

}