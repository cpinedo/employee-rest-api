package com.example.kenzan.repository;

import com.example.kenzan.model.Employee;

import org.springframework.data.repository.CrudRepository;

/**
 * EmployeeRepository
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}